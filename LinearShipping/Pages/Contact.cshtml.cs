﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace LinearShipping.Pages
{
    public class ContactModel : PageModel
    {
        public string ContactOneHeader { get; set; }
        public string ContactOneAddress { get; set; }
        public string ContactOneState { get; set; }
        public string ContactOnePhone { get; set; }
        public string ContactOneFax { get; set; }
        public string ContactOneMarketingEmail { get; set; }
        public string ContactOneSupportEmail { get; set; }

        public void OnGet()
        {
            ContactOneHeader = "Corporate Address.";
            ContactOneAddress = "9310 Ronda Ln";
            ContactOneState = "Houston TX 77074";
            ContactOnePhone = "713-643-7447";
            ContactOneFax = "713-643-5989";
            ContactOneMarketingEmail = "info@linearshipping.com";
            ContactOneSupportEmail = "info@linearshipping.com"; 
        }
    }
}
